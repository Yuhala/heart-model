#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include "data-set.h"

using namespace std;
#include <stdio.h>
#include <stdlib.h>
// If test is true we are reading test/validation data
void DataSet::ReadCSVFile(string dir, string file_name, bool test)
{
  // if cmake is used to build, the csv file will be next to the binary
  // - binary
  // - file.csv
  // if bazel is used, the csv file will be under some directories relative to the binary
  // - binary
  // - tensorflow/cc/models/file.csv
  const char *path = "/work/data/test-data.csv";

  printf("Testing fs shield...\n");
  FILE *fp =fopen(path,"r");
  char c;
    while((c=fgetc(fp))!=EOF){
        printf("%c",c);
    }
    fclose(fp);
    return;


  ifstream file(file_name);
  if (!file)
  {
    file.open(dir + file_name);
  }
  if (!file)
  {
    cerr << "ERROR: No " << file_name << " next to the binary or at " << dir
         << ", please double check the location of the CSV dataset file." << endl;
    cerr << "ERROR: The dir option must be relative to the binary location." << endl;
  }
  stringstream buffer;
  buffer << file.rdbuf();
  string line;
  vector<string> lines;
  while (getline(buffer, line, '\n'))
  {
    lines.push_back(line);
  }

  //if first line contains metadata; no metadata in this case

  /*
  vector<float> metadata = ReadCSVLine(lines[0]);
  data_set_metadata.age = metadata[0];
  data_set_metadata.sex = metadata[1];
  */

  //the other lines contain the heart data for each individual in the dataset
  //order:
  vector<float> temp;
  for (int i = 0; i < lines.size(); ++i)
  {
    vector<float> features = ReadCSVLine(lines[i]);

    if (!test)
    { //reading training data
      x_.insert(x_.end(), features.begin(), features.begin() + 13);
      y_.push_back(features[13]);
    }
    else
    {
      x_test.insert(x_test.end(), features.begin(), features.begin() + 13);
      y_test.push_back(features[13]);
    }
  }
}

vector<float> DataSet::ReadCSVLine(string line)
{
  vector<float> line_data;
  std::stringstream lineStream(line);
  std::string cell;

  while (std::getline(lineStream, cell, ','))
  {
    //setlocale(LC_ALL,"C");
    //double d = stod(cell);
    //std::cout << "double is: " << d << "\n";
    line_data.push_back(stod(cell));
  }
  //std::cout << "read csv line successfully\n";
  return line_data;
}

initializer_list<float> DataSet::input(float age,
                                       float sex,
                                       float cp,
                                       float trestbps,
                                       float chol,
                                       float fbs,
                                       float restecg,
                                       float thalach,
                                       float exang,
                                       float oldpeak,
                                       float slope,
                                       float ca,
                                       float thal)
{
  return {age, sex, cp, trestbps, chol, fbs, restecg, thalach, exang, oldpeak, slope, ca, thal};
}

initializer_list<float> test(const vector<float> &data, int index)
{
  vector<float> in;
  //DEBUG_PRINT("Test data: ");
  //in.insert(in.end(), data.at(index), data.at(index) + 13);
  for (int i = 0, j = index; i < 13; i++, j++)
  {
    in.push_back(data.at(j));
    //DEBUG_PRINT("%f ", in.at(i));
  }
  //DEBUG_PRINT("\n");
  //DEBUG_PRINT("Return this test ================================================\n");

  return {in[0], in[1], in[2], in[3], in[4], in[5], in[6], in[7], in[8], in[9], in[10], in[11], in[12]};
}
float DataSet::output(float res)
{
  //out is result of sigmoid activation function
  return res < 0.5 ? 0 : 1;
}