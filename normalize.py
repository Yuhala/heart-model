#
#Author: Peterson Yuhala
#Use: Normalize all columns in a dataset
#

from sklearn import preprocessing
import numpy as np
import pandas as pd

df = pd.read_csv("data/data-raw.csv",sep=",")
min_max_scaler = preprocessing.MinMaxScaler()
df_scaled = min_max_scaler.fit_transform(df)

# Run the normalizer on the dataframe
df_normalized = pd.DataFrame(df_scaled)
ds = df_normalized.sample(frac=1) #shuffle the rows
ds.to_csv("shuffled-norm.csv",sep=',',index=False) #save dataframe as csv file without index column
   
#print(normalized_x)