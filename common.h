/**
 * Author: Peterson Yuhala
 * Purpose: This file defines model parameter properties and training options
 * Modify this file according to your model structure
 * We work with 2D tensors for now. Higher dim tensors-->TODO
 */

#ifndef COMMON_H
#define COMMON_H

#include "tensorflow/core/public/session.h"
#include "tensorflow/core/protobuf/meta_graph.pb.h"

#include "tensorflow/cc/client/client_session.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/cc/framework/gradients.h"
#include "tensorflow/core/lib/gtl/array_slice.h"

//#include "tensorflow/core/platform/test.h"
//Libs for reading/writing files
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <random>

//#include <vector>
#include "checkpoint.h"
#include "crypto/crypto.h"
#include "data-set.h"
#include "romulus/romuluslog/RomulusLog.hpp"
#include "romulus/common/tm.h"

using namespace tensorflow;
using namespace tensorflow::ops;
using namespace tensorflow::gtl;
using namespace std;
using namespace romuluslog;

/* Define training options */
#define EPOCHS 100000 //40000  //Number of training iterations
#define USE_PMEMx   //Save model params on pmem
#define USE_CHKPT     //Save model params on traditional checkpoint files
#define BENCHMARKING //Define when doing benchmarking
#define DEBUG         //Defines debug mode for program

#ifdef DEBUGx
#define DEBUG_PRINT(...) printf(__VA_ARGS__)
#else
#define DEBUG_PRINT(...) \
  do                     \
  {                      \
  } while (0)
#endif

namespace trainingopts
{

const float learning_rate = 0.001;
const std::string base = "/work/data/";//"/home/ubuntu/peterson/tensorflow/tensorflow/cc/heart-model/";
const std::string pmem_base = "/pmem/";//"/mnt/pmem0/";
const std::string prefix = base;// + "checkpoints/";
const std::string results = "/work/results.csv";

} // namespace trainingopts
using namespace trainingopts;

/* We define the sizes of all weight and bias matrices corresponding to our model here */
//x: rows y: columns

#define W1x 13 // #neurons in input layer ie #input attributes
#define W1y 320 //20 // #neurons in 1'st hidden layer

#define W2x W1y
#define W2y 160 //10 // #neurons in 2'nd hidden layer

#define W3x W2y
#define W3y 1 //#neurons in output layer

/* Biases are 1xn matrices; we define just n */
#define B1 W1y
#define B2 W2y
#define B3 W3y

/* Total num of params in model */
#define NUM_PARAMS ((W1x * W1y) + (W2x * W2y) + (W3x * W3y) + B1 + B2 + B3)
/* Params size in MB */
#define PARAM_SIZE (NUM_PARAMS * sizeof(float))// lhs/2^20

//common routines

/**
 * Decrypts the data in the input tensor
 * T1: ciphertext type(eg unsigned char), T2: plaintext type(eg float)
 */
template <typename T1, typename T2>
void DecryptVals(Tensor *src, Tensor *dest, unsigned char *key)
{
  auto flat = src->flat<T1>();
  unsigned char ct[flat.size() * sizeof(T1)];
  unsigned char pt[sizeof(ct) - ADD_ENC_DATA_SIZE];
  DEBUG_PRINT("Size of ct: %d\n", sizeof(ct));
  memcpy(ct, flat.data(), sizeof(ct));
  aes_gcm_decrypt(pt, ct, key, sizeof(ct));

  auto flat2 = dest->flat<T2>();
  memcpy(flat2.data(), pt, sizeof(pt));
}

/* Copies values from src to dest tensor */
template <typename T>
void InitValsT(Tensor *dest, Tensor *src)
{
  auto flat1 = src->flat<T>();
  auto flat2 = dest->flat<T>();
  memcpy(flat2.data(), flat1.data(), flat2.size() * sizeof(T));

  /*
  for (int i = 0; i < flat2.size(); ++i)
    std::cout << "Init val: " << flat2(i) << "\n";
  */
}

/* Creates a tensor with values from C++ array */
template <typename T>
void InitVals(Tensor *tensor, T *arr, size_t size)
{
  auto flat = tensor->flat<T>();
  memcpy(flat.data(), arr, size * sizeof(T));
  /*
  std::cout << "Init flat size: " << flat.size() << "\n";

  for (int i = 0; i < flat.size(); ++i)
    std::cout << "Init val: " << flat(i) << "\n";
  */
}

/* Flattens tensor and writes its values to an array */
template <typename T>
void WriteVals(Tensor *tensor, T *arr, size_t size)
{
  auto flat = tensor->flat<T>();
  //std::cout<<"flat size: "<< flat.size()<< "Input size: "<<size<<"\n";
  memcpy(arr, flat.data(), size * sizeof(T));
  //for (int i = 0; i < flat.size(); ++i)
  //std::cout << "Write val: " << flat(i) << "\n";

  //for (int i = 0; i < flat.size(); ++i) flat(i) = val;
}
template <typename T>
void FillT(Tensor *tensor, const T val)
{
  auto flat = tensor->flat<T>();
  for (int i = 0; i < flat.size(); ++i)
    flat(i) = val;
}
template <typename T>
void PrintT(Tensor *tensor)
{
  auto flat = tensor->flat<T>();
  for (int i = 0; i < flat.size(); ++i)
    std::cout << "Value: " << flat(i) << "\n";
}
float myRand()
{
  //return ((float) rand() / (RAND_MAX));
  return 0.25;
}
#endif /* COMMON_H */