#ifndef DATA_SET_H
#define DATA_SET_H
using namespace std;

// Meta data used to normalize the data set. We normalize just target in our case:
//Xnorm = (X-Xmin)/(Xmax-Xmin)

//take input and output vector
initializer_list<float> test(const vector<float> &data, int index);
class DataSetMetaData
{
  friend class DataSet;
  //age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,ca,thal,target
private:
  float age;
  float sex; //male =1; female=0
  float cp;
  float trestbps;
  float chol;
  float fbs;
  float restecg;
  float thalach;
  float exang;
  float oldpeak;
  float slope;
  float ca;
  float thal;
  float target; //val to be predicted
};

enum class Sex
{
  FEMALE,
  MALE
};

class DataSet
{
public:
  // Construct a data set from the given csv file path.
  DataSet(string dir, string file_name, bool test)
  {
    ReadCSVFile(dir, file_name, test);
  }

  // getters
  vector<float> &x() { return x_; }
  vector<float> &y() { return y_; }
  vector<float> &xt() { return x_test; }
  vector<float> &yt() { return y_test; }

  // read the given csv file and complete x_ and y_
  void ReadCSVFile(string dir, string file_name, bool test);

  // convert one csv line to a vector of float
  vector<float> ReadCSVLine(string line);

  // take input and output vector
  initializer_list<float> input(float age,
                                float sex, //male =1; female=0
                                float cp,
                                float trestbps,
                                float chol,
                                float fbs,
                                float restecg,
                                float thalach,
                                float exang,
                                float oldpeak,
                                float slope,
                                float ca,
                                float thal);

  // output the result of a classification result
  float output(float res);

private:
  DataSetMetaData data_set_metadata;
  vector<float> x_;
  vector<float> y_;
  vector<float> x_test;
  vector<float> y_test;
};
#endif /* DATA_SET_H */