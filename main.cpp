/**
 * Author: Peterson Yuhala
 * Purpose: Secure tf training with checkpointing on persistent memory
 */

#include "heart-model.hpp"

//NB: key placed here in development mode. For production mode the key used is the scone file 
//system shield key. It will be injected into the application via CAS..see https://sconedocs.github.io/CASOverview/

unsigned char key[] = "12345678123456781234567812345678"; //32 byte / 256 bit key (AES-256-GCM)

int main()
{

  HeartModel model; //initialize heart model
  std::cout << "Training model...\n";
  model.train(key); //train model

  return 0;
}