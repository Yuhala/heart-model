/**
 * Author: Peterson Yuhala
 * Purpose: Code to implement checkpointing of trained params
 * N.B: We encrypt all params b4 checkpointing
 */

#include "checkpoint.h"
#include "crypto/crypto.h"

/* checkpoint params to disk/ssd */
void do_checkpoint(std::vector<Tensor> *addr, int epoch, std::string path, unsigned char *key)
{
    std::vector<Tensor> params = *addr;
    // name mapping between vector of Tensors and checkpoint name in checkpoint file...
    //     w1 --> chkpt0
    //     w2 --> chkpt1
    //     w3 --> chkpt2
    //     b1 --> chkpt3
    //     b2 --> chkpt4
    //     b3 --> chkpt5
    //

    // write trained parameters to checkpoint file
    tensorflow::checkpoint::TensorSliceWriter writer(
        path,
        tensorflow::checkpoint::CreateTableTensorSliceBuilder);

    unsigned int jdx = 0, x = 0, y = 0;

    std::string chkptname;
    tensorflow::TensorSlice tslice = tensorflow::TensorSlice::ParseOrDie("-:-");
    const float *train_param_rawdata = nullptr;
    const unsigned char *enc_data = nullptr;
    for (tensorflow::Tensor t : params)
    {
        train_param_rawdata = reinterpret_cast<const float *>(t.tensor_data().data());
        x = t.dim_size(0);
        y = t.dim_size(1);
        tensorflow::TensorShape shape({1, x * y * sizeof(float) + ADD_ENC_DATA_SIZE}); //1 x #bytes total
        chkptname = std::string("chkpt") + std::to_string(jdx);
        //Encrypt values b4 writing checkpoints

        unsigned char pt[x * y * sizeof(float)];          //plaintext param buffer
        unsigned char ct[sizeof(pt) + ADD_ENC_DATA_SIZE]; //ciphertext param buffer
        memcpy(pt, train_param_rawdata, sizeof(pt));
        aes_gcm_encrypt(pt, ct, key, sizeof(pt));
        enc_data = reinterpret_cast<const unsigned char *>(ct);

        TF_CHECK_OK(
            writer.Add<unsigned char>(chkptname, shape, tslice, enc_data));
        jdx++;
    }
    //checkpoint training step
    Tensor ten(DT_FLOAT, TensorShape({1, 1}));
    ten.flat<float>()(0) = epoch; //ten is a 1x1 tensor so its flat version contains just one elt..
    train_param_rawdata = reinterpret_cast<const float *>(ten.tensor_data().data());
    tensorflow::TensorShape shape({1, 1});
    chkptname = std::string("chkpt_step");
    TF_CHECK_OK(
        writer.Add<float>(chkptname, shape, tslice, train_param_rawdata));

    TF_CHECK_OK(writer.Finish());
    //std::cout << "Written checkpoints successfully..\n";
}