/**
 * Author: Peterson Yuhala
 */

#include "common.h"
#include "nv-model.hpp"

/* DNN for heart disease prediction based on logistic regression */
class HeartModel
{

private:
    int training_level{0};

    std::unordered_map<std::string, std::string> hyparams; //used for checkpointing
    std::vector<Output> weights;                           //model's weight variables
    std::vector<Output> biases;                            //model's bias variables
    std::vector<Output> layers;                            //model's layers
    Output loss;                                           //loss tensor
    Output step;                                           //checkpoint step/epoch variable

    Scope scope; //contains model's graph
    GraphDef graph;
    bool chkpt_exists{false};

public:
    HeartModel() : scope(Scope::NewRootScope())
    {
        //---------------------initializing variables--------------------------

        //allocate checkpoint epoch/step variable
        step = Variable(scope.WithOpName("step"), {1, 1}, DT_INT64);
        //training_level = 0;

        //allocate model weights
        Output w1 = Variable(scope.WithOpName("w1"), {W1x, W1y}, DT_FLOAT);
        Output w2 = Variable(scope.WithOpName("w2"), {W2x, W2y}, DT_FLOAT);
        Output w3 = Variable(scope.WithOpName("w3"), {W3x, W3y}, DT_FLOAT);

        //add weights to weights vector

        weights.push_back(w1);
        weights.push_back(w2);
        weights.push_back(w3);

        //allocate model biases
        Output b1 = Variable(scope.WithOpName("b1"), {1, B1}, DT_FLOAT);
        Output b2 = Variable(scope.WithOpName("b2"), {1, B2}, DT_FLOAT);
        Output b3 = Variable(scope.WithOpName("b3"), {1, B3}, DT_FLOAT);

        //add biases to biases vector
        biases.push_back(b1);
        biases.push_back(b2);
        biases.push_back(b3);

        std::cout << "Allocated weights and biases...\n";

        //-----------------------------------------------------------------------
    }
    ~HeartModel()
    {
        //cleanup
    }

    void train(unsigned char *key)
    {
        std::string chkpt_file = trainingopts::prefix + "trained_param_checkpoints.ckpt";
#ifdef USE_PMEM
        std::cout << "Training in pmem mode: params flushed to pmem\n";
#endif
#ifdef USE_CHKPT
        hyparams["trained_param_path"] = chkpt_file;

        std::cout << "Training in traditional checkpoint mode: params will be saved on checkpoint files\n";

        //std::cout << "Define a parameter saving mode: pmem or chkpt mode\n";
        //exit();
#endif

        ifstream fs(chkpt_file);
        if (fs)
        {
            chkpt_exists = true;
        }
        //training session
        ClientSession session(scope);

        //training data objects
        DataSet data_set(base, "heart-data-uci.csv", false);
        Tensor x_data(DataTypeToEnum<float>::v(),
                      TensorShape{static_cast<int>(data_set.x().size()) / 13, 13});
        std::cout << "X data size is: " << data_set.x().size() << "\n";
        copy_n(data_set.x().begin(), data_set.x().size(),
               x_data.flat<float>().data());

        Tensor y_data(DataTypeToEnum<float>::v(),
                      TensorShape{static_cast<int>(data_set.y().size()), 1});
        std::cout << "Y data size is: " << data_set.y().size() << "\n";
        copy_n(data_set.y().begin(), data_set.y().size(),
               y_data.flat<float>().data());

        std::cout << "Done obtaining training data\n";

        std::vector<Tensor *> paramTensors; //contains tensors restored from checkpoint/pmem

        // input and output tensors
        auto x = Placeholder(scope, DT_FLOAT);
        auto y = Placeholder(scope, DT_FLOAT);

        // tensor for storing checkpoint step
        Tensor ten(DT_INT64, TensorShape({1, 1}));
        ten.scalar<int>()() = 0;

        //NB: push the following tensors into the vector in this exact order !
        // weight tensors
        Tensor tw1(DT_FLOAT, TensorShape({W1x, W1y}));
        Tensor tw2(DT_FLOAT, TensorShape({W2x, W2y}));
        Tensor tw3(DT_FLOAT, TensorShape({W3x, W3y}));
        paramTensors.push_back(&tw1);
        paramTensors.push_back(&tw2);
        paramTensors.push_back(&tw3);

        // bias tensors
        Tensor tb1(DT_FLOAT, TensorShape({1, B1}));
        Tensor tb2(DT_FLOAT, TensorShape({1, B2}));
        Tensor tb3(DT_FLOAT, TensorShape({1, B3}));
        paramTensors.push_back(&tb1);
        paramTensors.push_back(&tb2);
        paramTensors.push_back(&tb3);

        //Add variable names to names vector; push back in order
        std::vector<string> vNames = {"w1", "w2", "w3", "b1", "b2", "b3"};
        /*
        vNames.push_back("w1"); //0
        vNames.push_back("w2"); //1
        vNames.push_back("w3"); //2
        vNames.push_back("b1"); //3
        vNames.push_back("b2"); //4
        vNames.push_back("b3"); //5 
        */

#ifdef USE_PMEM

        /* Test if persistent weights exist; create nonvolatile model if not */
        NVModel *nvModel = RomulusLog::get_object<NVModel>(0);
        if (nvModel == nullptr)
        {

            nvModel = (NVModel *)TM_PMALLOC(sizeof(struct NVModel::Layer));
            RomulusLog::put_object(0, nvModel);
            nvModel->allocate_model<unsigned char, float>();
            std::cout << "Allocated persistent model successfully\n";

            FillT<float>(&tw1, myRand());
            FillT<float>(&tw2, myRand());
            FillT<float>(&tw3, myRand());

            //Biases are set to zero
            FillT<float>(&tb1, 0.0);
            FillT<float>(&tb2, 0.0);
            FillT<float>(&tb3, 0.0);

            //assign tensors to weights and biases
            auto assign_w1 = Assign(scope.WithOpName("assign_w1"), weights[0], tw1);
            auto assign_w2 = Assign(scope.WithOpName("assign_w2"), weights[1], tw2);
            auto assign_w3 = Assign(scope.WithOpName("assign_w3"), weights[2], tw3);

            auto assign_b1 = Assign(scope.WithOpName("assign_b1"), biases[0], tb1);
            auto assign_b2 = Assign(scope.WithOpName("assign_b2"), biases[1], tb2);
            auto assign_b3 = Assign(scope.WithOpName("assign_b3"), biases[2], tb3);

            //run the assign operations in the training session
            TF_CHECK_OK(session.Run({assign_w1, assign_w2, assign_w3, assign_b1, assign_b2, assign_b3}, nullptr));
            std::cout << "Initialized params with random values\n";
        }
        else
        {

            std::cout << "Persistent model exists in pmem\n";

            auto start_pmem = chrono::steady_clock::now(); //benchmarking restore time
            training_level = nvModel->epoch;               //resume training at this level
            //Read values from pmem model into param tensors
            nvModel->read_params<float>(paramTensors, key);

            std::cout << "Constructing tensors from pmem data\n";
            //assign param tensors to weights and biases
            //1. assign weights

            auto assign_nvw1 = Assign(scope.WithOpName("assign_nvw1"), weights[0], tw1);
            auto assign_nvw2 = Assign(scope.WithOpName("assign_nvw2"), weights[1], tw2);
            auto assign_nvw3 = Assign(scope.WithOpName("assign_nvw3"), weights[2], tw3);

            //2.assign biases

            auto assign_nvb1 = Assign(scope.WithOpName("assign_nvb1"), biases[0], tb1);
            auto assign_nvb2 = Assign(scope.WithOpName("assign_nvb2"), biases[1], tb2);
            auto assign_nvb3 = Assign(scope.WithOpName("assign_nvb3"), biases[2], tb3);

            // run the assign operations in the training sesssion
            TF_CHECK_OK(session.Run({assign_nvw1, assign_nvw2, assign_nvw3, assign_nvb1, assign_nvb2, assign_nvb3}, nullptr));

            auto stop_pmem = chrono::steady_clock::now();
            auto pmem_time = stop_pmem - start_pmem;

            std::cout << "Initialized params with persistent values in : " << chrono::duration<double, milli>(pmem_time).count() << " ms. Will resume training at epoch: " << training_level << "\n";
        }
#endif

#ifdef USE_CHKPT
        //restore checkpointed params from checkpoint file binary
        //if checkpoints exist load checkpointed params; to estimate restore time measure the time of this if statement...
        if (chkpt_exists)
        {

            //Load checkpointed parameters
            // name mapping between vector of Tensors and checkpoint name in checkpoint file...
            //     w1 --> chkpt0
            //     w2 --> chkpt1
            //     w3 --> chkpt2
            //     b1 --> chkpt3
            //     b2 --> chkpt4
            //     b3 --> chkpt5
            //     step --> chkpt_step
            auto start_chkpt = chrono::steady_clock::now(); //benchmarking restore time

            //--------------------------------start checkpoint restore--------------------
            std::vector<std::string> tensor_names = {"chkpt0", "chkpt1", "chkpt2", "chkpt3", "chkpt4", "chkpt5", "chkpt_step"};

            //Get all the tensors at index 0 to 6
            tensorflow::checkpoint::TensorSliceReader reader(hyparams["trained_param_path"]);
            TF_CHECK_OK(reader.status());
            std::unique_ptr<tensorflow::Tensor> out_t;
            std::vector<Tensor *> paramTensors = {&tw1, &tw2, &tw3, &tb1, &tb2, &tb3};
            Tensor *temp1, *temp2;
            int x = 0, y = 0;

            for (int i = 0; i < 6; i++)
            {
                reader.GetTensor(tensor_names.at(i), &out_t);
                temp1 = out_t.release();
                x = (*paramTensors.at(i)).dim_size(0);
                y = (*paramTensors.at(i)).dim_size(1);
                Tensor dest(DT_FLOAT, TensorShape({x, y}));
                DecryptVals<unsigned char, float>(temp1, &dest, key);
                InitValsT<float>(paramTensors.at(i), &dest);
            }

            reader.GetTensor(tensor_names.at(6), &out_t); //training level
            temp1 = out_t.release();

            training_level = (int)temp1->flat<float>()(0);
            //------------------------------------

            //training_level = 0; //testing
            //std::cout << "Saved training level is: " << training_level << "\n";

            //assign tensors to weights and biases
            auto assign_w1 = Assign(scope.WithOpName("assign_w1"), weights[0], tw1);
            auto assign_w2 = Assign(scope.WithOpName("assign_w2"), weights[1], tw2);
            auto assign_w3 = Assign(scope.WithOpName("assign_w3"), weights[2], tw3);

            auto assign_b1 = Assign(scope.WithOpName("assign_b1"), biases[0], tb1);
            auto assign_b2 = Assign(scope.WithOpName("assign_b2"), biases[1], tb2);
            auto assign_b3 = Assign(scope.WithOpName("assign_b3"), biases[2], tb3);

            //run assign operation in training session
            TF_CHECK_OK(session.Run({assign_w1, assign_w2, assign_w3, assign_b1, assign_b2, assign_b3}, nullptr));
            //----------------------------------end restore------------------------
            auto stop_chkpt = chrono::steady_clock::now();
            auto chkpt_time = stop_chkpt - start_chkpt;
            std::cout << "Loaded saved params from checkpoints successfully in: " << chrono::duration<double, milli>(chkpt_time).count() << " ms\n";
            std::cout << "Will resume training at : " << training_level << " epoch\n";
        }
        //checkpoint does not exist initialize params with random normal values
        else
        {
            /**
             * Fill the tensors with random vals btw 0 and 1
             * Not doing this may lead to very wrong values eg Nan,Inf 
             */

            /**
             * Weights should not be zero, else model would be stuck as the learning algorithm 
             * would not make any changes to the model. ie n*0=0.
            */

            FillT<float>(&tw1, myRand());
            FillT<float>(&tw2, myRand());
            FillT<float>(&tw3, myRand());

            //Bias can be set to zero
            FillT<float>(&tb1, 0.0);
            FillT<float>(&tb2, 0.0);
            FillT<float>(&tb3, 0.0);

            //assign tensors to weights and biases
            auto assign_w1 = Assign(scope.WithOpName("assign_w1"), weights[0], tw1);
            auto assign_w2 = Assign(scope.WithOpName("assign_w2"), weights[1], tw2);
            auto assign_w3 = Assign(scope.WithOpName("assign_w3"), weights[2], tw3);

            auto assign_b1 = Assign(scope.WithOpName("assign_b1"), biases[0], tb1);
            auto assign_b2 = Assign(scope.WithOpName("assign_b2"), biases[1], tb2);
            auto assign_b3 = Assign(scope.WithOpName("assign_b3"), biases[2], tb3);

            //Init the weights and biases by running the assigns nodes once
            TF_CHECK_OK(session.Run({assign_w1, assign_w2, assign_w3, assign_b1, assign_b2, assign_b3}, nullptr));
            std::cout << "Initialized params with random values\n";
        }

#endif

        //build model layers with parameter matrices: layer(i)= weights(i)*layer(i-1)+biases(i)

        // layers
        //auto layer_1 = Tanh(scope, Tanh(scope, Add(scope, MatMul(scope, x, w1), b1)));
        //create layers and add to layers vector
        Output layer1 = Relu(scope, Add(scope, MatMul(scope, x, weights[0]), biases[0]));
        layers.push_back(layer1);
        Output layer2 = Relu(scope, Add(scope, MatMul(scope, layers[0], weights[1]), biases[1]));
        layers.push_back(layer2);
        Output layer3 = Sigmoid(scope, Add(scope, MatMul(scope, layers[1], weights[2]), biases[2])); //classification layer: 0 < val < 1
        layers.push_back(layer3);

        //auto layer_pred = Sigmoid(scope,Add(scope, MatMul(scope, layer_2, w3), b3));

        //classification model ends here; the following operations below are done only on the training model.

        // regularization
        auto regularization = AddN(scope,
                                   initializer_list<Input>{L2Loss(scope, weights[0]),
                                                           L2Loss(scope, weights[1]),
                                                           L2Loss(scope, weights[2])});

        // loss calculation
        loss = Add(scope,
                   ReduceMean(scope, Square(scope, Sub(scope, layers[2], y)), {0, 1}),
                   Mul(scope, Cast(scope, trainingopts::learning_rate, DT_FLOAT), regularization));

        //auto cost = SoftmaxCrossEntropyWithLogits(scope, layer_3, y);

        //cost = ReduceMean(root, Square(root, Sub(root, out_node, y_labels)), {0, 1});
        /* auto loss = Add(scope, cost,
                  Mul(scope, Cast(scope, 1.0e-4, DT_FLOAT), regularization));*/

        // add the gradients operations to the graph
        std::vector<Output> grad_outputs;
        TF_CHECK_OK(AddSymbolicGradients(scope, {loss}, {weights[0], weights[1], weights[2], biases[0], biases[1], biases[2]}, &grad_outputs));

        // update the weights and bias using gradient descent . TODO: do this in a loop

        auto apply_w1 = ApplyGradientDescent(scope, weights[0], Cast(scope, 0.01, DT_FLOAT), {grad_outputs[0]});
        auto apply_w2 = ApplyGradientDescent(scope, weights[1], Cast(scope, 0.01, DT_FLOAT), {grad_outputs[1]});
        auto apply_w3 = ApplyGradientDescent(scope, weights[2], Cast(scope, 0.01, DT_FLOAT), {grad_outputs[2]});
        auto apply_b1 = ApplyGradientDescent(scope, biases[0], Cast(scope, 0.01, DT_FLOAT), {grad_outputs[3]});
        auto apply_b2 = ApplyGradientDescent(scope, biases[1], Cast(scope, 0.01, DT_FLOAT), {grad_outputs[4]});
        auto apply_b3 = ApplyGradientDescent(scope, biases[2], Cast(scope, 0.01, DT_FLOAT), {grad_outputs[5]});

        std::vector<Tensor> outputs;
        std::vector<Tensor> params;

        //for benchmarking
#ifdef BENCHMARKING
        // double diff = 0;
        ofstream outputFile;

        outputFile.open(results, ios::out | ios::app);
        outputFile << "Epoch/Training Level"
                   << ","
                   << "Loss"
                   << "\n";
        outputFile.flush();
#endif
        auto start = chrono::steady_clock::now();
        /* Resumed at saved training level on pmem; zero by default */
        for (int i = training_level; i < EPOCHS; ++i)
        {

            //std::cout << "In training loop\n";
            //test

            if (i % 500 == 0)
            {
                // TF_CHECK_OK(session.Run({{x, x_data}, {y, y_data}}, {loss}, &outputs));
                // std::cout << "Loss after " << i << " steps " << outputs[0].scalar<float>() << std::endl;
            }

#ifdef BENCHMARKING
            //auto start = chrono::steady_clock::now();
#endif
            // nullptr because the output from the run is useless
            TF_CHECK_OK(session.Run({{x, x_data}, {y, y_data}}, {apply_w1, apply_w2, apply_w3, apply_b1, apply_b2, apply_b3}, nullptr));

#ifdef BENCHMARKING
            //auto stop = chrono::steady_clock::now();
            //diff = difftime(stop,start);
            //auto tmp = stop - start;
            //diff = chrono::duration<double, milli>(tmp).count();

            if (i % 5000 == 0)
            {
                TF_CHECK_OK(session.Run({{x, x_data}, {y, y_data}}, {loss}, &outputs));
                std::cout << "Loss after " << i << " steps " << outputs[0].scalar<float>() << std::endl;
                outputFile << i << "," << outputs[0].scalar<float>() << "\n";
                outputFile.flush();
            }
#endif

#ifdef USE_PMEM
            /* Extract weight and bias variables into tensors */
            TF_CHECK_OK(session.Run({weights[0], weights[1], weights[2], biases[0], biases[1], biases[2]}, &params));

            //PrintT<float>(&temp[0]);

            //flush params to persistent model. NB: this is done in 1 transaction in pmem;

            //flush params is template: T1 for encrypted data type and T2 for decrypted data type
            nvModel->flush_params<unsigned char, float>(params, i, key); //On restore, resume from i+1
#endif

#ifdef USE_CHKPT

            /* Extract weight and bias variables into tensors */
            TF_CHECK_OK(session.Run({weights[0], weights[1], weights[2], biases[0], biases[1], biases[2]}, &params));
            do_checkpoint(&params, i, hyparams["trained_param_path"], key);

#endif
        }
        auto stop = chrono::steady_clock::now();
        auto diff = stop - start;
        std::cout << "Total training time for " << EPOCHS << " epochs: " << chrono::duration<double, milli>(diff).count() << " ms\n";
        printf("Size of params: %d", PARAM_SIZE);

#ifdef BENCHMARKING
        outputFile.close();
#endif
        //stop training timer here..TODO

        /**
         * Re-Run graph again; Done because variable extraction in the above loop seems to change 
         * some aspects of the graph producing wrong predictions e.g nans and infs
         */
        TF_CHECK_OK(session.Run({{x, x_data}, {y, y_data}}, {apply_w1, apply_w2, apply_w3, apply_b1, apply_b2, apply_b3}, nullptr));

    validate:
        /**
         * Here we test the models performance in predicting values for the 
         * test data set
         */
        DataSet test_data(base, "test-data.csv", true);
        std::cout << "Starting validation...\n";
        int correct = 0;
        float accur = 0;
        int size = test_data.xt().size();
        int num_elts = size / 13;
        DEBUG_PRINT("Test data size is :%d Number of test inputs: %d\n", size, num_elts);
        const vector<float> temp = test_data.xt();
        for (int idx = 0, j = 0; idx < size; idx += 13, j++)
        { //Take 13 elts at a time from x_test vector

            TF_CHECK_OK(session.Run({{x, {test(temp, idx)}}}, {layers[2]}, &outputs));
            if (data_set.output(*outputs[0].scalar<float>().data()) - test_data.yt().at(j) == 0)
            {
                correct++;
            }
        }

        accur = ((float)correct / (float)num_elts) * 100;
        std::cout << "Model Accuracy: " << accur << "%\n";

        //saving the model
        //GraphDef graph_def;
        //TF_ASSERT_OK(scope.ToGraphDef(&graph_def));
        //Clean up: Free allocated memory TODO

        return;
    }

    /* do predictions based on trained model at the prediction layer */
    void predict(Output pred_layer) //TODO
    {
        //prediction session
        //ClientSession p_session(scope);
    }

    /* validate model */
    float validate(Output pred_layer, std::vector<Tensor> *addr) //TODO
    {
        return 0;
    }
};
