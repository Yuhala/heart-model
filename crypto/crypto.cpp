/**
 * Author: Peterson Yuhala
 * Purpose: AES-256-GCM cryptography algorithms
 */

#include "crypto.h"
/* AES-256-GCM encryption algorithm */
void aes_gcm_encrypt(unsigned char *pt, unsigned char *ct, unsigned char *key, size_t pt_len)
{
    EVP_CIPHER_CTX *ctx;
    int outlen, maclen;

    //temporary buffer holding encryption info: beware of stack smashing/buffer overflows..
    unsigned char outbuf[pt_len + ADD_ENC_DATA_SIZE]; //max size possible is pt_len + ADD_ENC_DATA_SIZE
    unsigned char iv[IV_SIZE];
    int res = RAND_bytes(iv, 12); //generate random iv; NIST recommendation
    //printf("IV is: %s\n", iv);
    /* Create evp context */
    ctx = EVP_CIPHER_CTX_new();
    /* Set cipher type and mode */
    EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
    /* Initialise key and IV */
    EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv);
    /* Encrypt plaintext */
    EVP_EncryptUpdate(ctx, outbuf, &outlen, pt, pt_len);
    /* Append encrypted block to ciphertext */
    memcpy(ct, outbuf, outlen);
    /* Append IV to ciphertext */
    memcpy(ct + outlen, iv, IV_SIZE);   
    /* Finalise: note get no output for GCM */
    EVP_EncryptFinal_ex(ctx, outbuf, &maclen);
    /* Get MAC tag */
    EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_GET_TAG, MAC_SIZE, outbuf);
    /* Append MAC to ciphertext*/
    memcpy(ct + outlen + IV_SIZE, outbuf, MAC_SIZE);
    /* ==> Complete ciphertext = encrypted_block + IV + MAC */
     /* Output encrypted block */
    //printf("Complete ciphertext: %s\n", ct);
    /* Free evp context */
    EVP_CIPHER_CTX_free(ctx);
}

/* AES-256-GCM decryption algorithm */
void aes_gcm_decrypt(unsigned char *res, unsigned char *ct, unsigned char *key, size_t ct_len)
{
    EVP_CIPHER_CTX *ctx;
    int outlen, tmplen, rv;
    int enc_block_len = ct_len - ADD_ENC_DATA_SIZE;
    //printf("AES decrypt size of ct: %d\n", ct_len);
    unsigned char outbuf[ct_len]; //temp buffer containing encryption info; max size possible is: ct_len
    unsigned char mac[MAC_SIZE];
    ctx = EVP_CIPHER_CTX_new();
    /* Select cipher */
    EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
    /* Extract iv from ciphertext */
    unsigned char iv[IV_SIZE];
    memcpy(iv, ct + enc_block_len, IV_SIZE);
    /* Specify key and IV */
    EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv);
    /* Decrypt plaintext */
    EVP_DecryptUpdate(ctx, outbuf, &outlen, ct, enc_block_len);
    /* Copy to result buffer */
    memcpy(res, outbuf, outlen);
    /* Extract MAC tag and authenticate ciphertext */
    memcpy(mac, ct + enc_block_len + IV_SIZE, MAC_SIZE);
    EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_TAG, MAC_SIZE, 
                        (void *)mac); //AEAD to CCM
    /* Compare tags */
    rv = EVP_DecryptFinal_ex(ctx, outbuf, &outlen);
    //printf("Tag Verify %s\n", rv > 0 ? "Successful!" : "Failed!");
    EVP_CIPHER_CTX_free(ctx);
}
