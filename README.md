## Summary
- The aim of this project is to use nvram to speed-up/improve training time of neural network models in tensorflow. We do this by flushing model parameters on nvram and restoring the latter parameters back into main memory following a crash. This approach proves to be more effective than traditional checkpointing of paramters to disk.
- We will then integrate scone with this project to provide fast secure training functionality for tensorflow models. 
- The example neural network used in this project is a `deep neural network` for `heart disease prediction` based on logistic regression. The data set used for training and validation can be found [here](https://archive.ics.uci.edu/ml/datasets/Heart+Disease). 
- This model serves as a proof of concept and so its prediction accuracy is not an important measure for us. Nevertheless, we are still optimizing the model i.e tuning the different hyperparameters (learning rate, #neurons per layer, #training epochs etc) to improve its prediction accuracy. 

## Quick background on how artificial neural networks work
### Structure of ANNs
- An artificial neural network is simply a mathematical model consisting of multiple `layers` each containing a number of `nodes/neurons`. Neurons on a given layer perform operations, based on an `activation/transfer function`, on data and output this data to neurons on the following layer. The information passed from one neuron to another characterizes/govern the behaviour of the whole network. These parameters are known as `weights` and `biases`. 
- Tensorflow stores these parameters in `matrices/tensors`. For example, `W(i,i+1)` would store weight paramters between layer(i) and layer(i+1) while `B(i)` will store bias parameters of layer(i). 
- For a given node/neuron on layer(i), its value y is given by: `y = AF[W(i-1)*x(i) + b(i)]`, where `AF` is an activation function like `Tanh`, `Sigmoid`, `Softmax` etc.
- The figure below ([Ref](https://matrices.io/deep-neural-network-from-scratch/)) represents an ANN with 4 layers: 1 `input` layer, 2 `hidden` layers and 1 `output` layer. 
- ![Example ANN](images/dnn.png) 
 

### Training process
- The aim of the training process for an ANN is to modify the different parameters such that the model produces correct/near correct values/predictions for new data. 
- Large models could take very long to train. This means one could lose the values of parameters of a model following a fault during the training process. Tensorflow solves this problem by providing `checkpointing` functionality. Tensorflow checkpoints capture the exact value of all paramters used by a model and saves them to checkpoint files on disk and restores the parameters after the crash. However, the high access times of disk could make this process very expensive.
- We propose to solve this problem using nvram. Our idea is to have persistent parameters for each model. During training, the values of trained parameters at a specific point are flushed to nvram. They are restored back into memory upon recovery. 
- Tests so far show our approach can be upto `3x` (and more..still to test) faster than traditional checkpointing, depending on the size of the model (# of parameters).

## Building a model using our approach
- To begin, tensorflow provides stable `Python` and `C++` APIs. For easier integration with scone and our persistent memory libraries, we default to using the C++ API. 
- We create persistent copy of our model's weights and biases. In persistent memory we represent a neural network model's parameters as a linked list of layers where `layer(i)` is a structure containing `W(i,i+1)` and `B(i+1)`, `i >= 0`. So a model with n hidden layers has n+1 weight and bias matrices, and hence n+1 layers in its persistent parameter model on nvram. The model on nvram stores only the values of these parameters together with the corresponding training epoch.
- The example heart disease prediction model built here has 3 layers. Its persistent parameter model is described in the [nv-model.hpp](nv-model.hpp).
- The number of neurons on each model layer is easily configurable in [common.h](common.h). `Wix` and `Wiy` represent the number of neurons on layer(i) and layer(i+1) respectively. The `learning rate` and other model hyperparameters are also defined in common.h file.
- The main model is defined in [heart-model.hpp](heart-model.hpp). We use `romulus` persistence library for all persistence related work.
- The [training data](heart-data-uci.csv) and [validation data](test-data.csv) were both normalized before passing them into the network.
- We use [crash.py](crash.py) to simulate crashes during the training process. 

## Training the model
- Before training, define the parameter saving method using `USE_CHKPT` or `USE_PMEM` in common.h to specify whether params are checkpointed on disk or flushed to nvram/pmem.

## Testing this program
- Clone the tensorflow repo into your environment and checkout to a specific version.
- Install bazel for the corresponding tf version: For example `tf version 1.12.0` is compatible with `bazel version 0.19.2`. 
- Modify all absolute paths in the project files: `heart-model.hpp` and `common.h` to reflect your own environment.
- Launch the `configure.sh` script in the tensorflow folder and choose the default configs. Clone this repo in the `/tensorflow/tensorflow/cc/` folder.
- In the project folder ie `/tensorflow/tensorflow/cc/heart-model` build and run the project using: `bazel run -c opt //tensorflow/cc/heart-model:heart-model`.
- To simulate crashes while the program is running (during training) launch the [crash.py](crash.py) script. The training process will be killed and restarted randomly until training completes.

## Scone integration
- The purpose of scone integration is to add the security aspect, i.e run the program in an SGX enclave.  
- The toolchain for building the project in a scone container is described in this project: [scone-pmem](https://gitlab.com/Yuhala/scone-pmem).
- We use an alpine3.7 image with the scone runtime preinstalled as our base image for our container. Our application image is built according to the toolchain described in above project. The tensorflow-pmem training application is built/compiled with the scone crosscompiler, which is musl based.
- To launch the application inside the enclave, we run the app in the SCONE runtime environment.
- Scone links our application's binary dynamically to the compiled tensorflow shared library; This is possible because scone supports dynamic loading of shared libraries.
### Security of trained parameters and training data
- All the data we write on nvram is pre-encrypted using AES-256-GCM. An 96 bit `IV` is generated (using openssl's `RAND_bytes`) for each encryption operation according to the NIST recommendation. For integrity verification we use a 128 bit `MAC`. Both the MAC and IV are appended to the ciphertext. So each ciphertext is of the form: `Encrypted-Block + IV + MAC`. Note: the IV and MAC are in plaintext, but this does not affect the security of the encrypted block in any way (Ref. NIST AES-GCM).
- The training data is encrypted with scone's [file system shield](https://sconedocs.github.io/SCONE_Fileshield/) during image creation. The training data is decrypted inside the enclave once a container based on the image is launched.
- A 256 bit encryption key is used for all encryption operations. 








