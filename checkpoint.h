/**
 * Author: Peterson Yuhala
 * Purpose: Code to implement checkpointing of trained params
 */

#ifndef CHECK_PT_H
#define CHECK_PT_H
#include "tensorflow/core/util/tensor_slice_writer.h" //for writing chkpts
#include "tensorflow/core/util/tensor_slice_reader.h" //for reading chkpts

using namespace tensorflow;


/* checkpoint params to disk/ssd */
void do_checkpoint(std::vector<Tensor> *addr, int epoch, std::string path,unsigned char* key);

#endif /* CHECK_PT_H */